package ru.sber.homework;

import ru.sber.common.CustomArray;
import java.util.Arrays;
import java.util.Collection;

public class CustomArrayImpl<T> implements CustomArray<T> {

    private Object[] data;
    private int size;
    private static final int DEFAULT_CAPACITY = 10;

    public CustomArrayImpl() {
        this.data = new Object[DEFAULT_CAPACITY];
        this.size = 0;
    }

    public CustomArrayImpl(int initialCapacity) {
        this.data = new Object[initialCapacity];
        this.size = 0;
    }

    public CustomArrayImpl(Collection<? extends T> data) {
        this.data = data.toArray();
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean add(T item) {
        ensureCapacity(1);
        data[size] = item;
        size++;
        return true;
    }

    @Override
    public boolean addAll(T[] items) {
        ensureCapacity(items.length);
        for(T item : items){
            add(item);
        }
        return items.length != 0;
    }

    @Override
    public boolean addAll(Collection<T> items) {
        Object[] item = items.toArray();
        ensureCapacity(item.length);
        for(T item1 : items){
            add(item1);
        }
        return data.length != 0;
    }

    @Override
    public boolean addAll(int index, T[] items) {
        if (index >= size) {
            throw new ArrayIndexOutOfBoundsException("");
        } else if(index < 0){
            throw new IllegalArgumentException("");
        }
        int indexData = index + items.length;
        int indexItems = 0;
        int j = indexData;
        ensureCapacity(items.length);
        for (int i = index; i <j; i++) {
            if((data.length - indexData) != 0) {
                data[indexData++] = data[i];
            }
            data[i] = items[indexItems++];
            size++;
        }
        return items.length != 0;
    }

    @Override
    public T get(int index) {
        if (index >= size) {
            throw new ArrayIndexOutOfBoundsException("");
        } else if(index < 0){
            throw new IllegalArgumentException("");
        }
        return (T) data[index];
    }

    @Override
    public T set(int index, T item) {
        if (index >= size) {
            throw new ArrayIndexOutOfBoundsException("");
        } else if(index < 0){
            throw new IllegalArgumentException("");
        }
        data[index]=item;
        return (T) data[index];
    }

    @Override
    public void remove(int index) {
        if (index >= size) {
            throw new ArrayIndexOutOfBoundsException("");
        } else if(index < 0){
            throw new IllegalArgumentException("");
        }
        else {
            for (int i = 1; i < size; i++) {
                if (index == i - 1) {
                    data[i - 1] = data[i];
                    index++;
                }
            }
            data[size - 1] = null;
            this.size--;
        }
    }

    @Override
    public boolean remove(T item) {
        int index = 0;
        for(int i = 1; i<size;i++) {
            if (item == data[i-1]) {
                data[i-1] = data[i];
                data[i] = item;
                index++;
            }
        }
        if(index!=0||item==data[size - 1]) {
            data[size - 1] = null;
            size--;
            return true;
        }
        return false;
    }

    @Override
    public boolean contains(T item) {
        for (int i = 0; i < size; i++) {
            if (item == data[i]) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int indexOf(T item) {
        if (item == null) {
            for (int i = 0; i < size; i++) {
                if (data[i] == null) {
                    return i;
                }
            }
        } else{
            for (int i = 0; i < size; i++) {
                if (item == data[i]) {
                    return i;
                }
            }
        }
        return -1;
    }

    @Override
    public void ensureCapacity(int newElementsCount) {
        int totalElementCount = size + newElementsCount;
        if(totalElementCount<=this.data.length){
            return;
        }
        int newCapacity = this.data.length * 2;
        if(totalElementCount>newCapacity){
            newCapacity = totalElementCount;
        }

        Object[] newData = new Object[newCapacity];
        for (int i = 0; i < size; i++) {
            newData[i] = data[i];
        }
        this.data = newData;
    }

    @Override
    public int getCapacity() {
        return this.data.length;
    }

    @Override
    public void reverse() {
        Object obj;
        for (int i = 0; i < size/2; i++) {
            obj = data[i];
            data[i] = data[size - i - 1];
            data[size - i - 1] = obj;
        }
    }

    @Override
    public Object[] toArray() {
        Object[] obj = new Object[size];
        for (int i = 0; i < size; i++){
            obj[i] = data[i];
        }
        return obj;
    }
    @Override
    public String toString() {
        return "CustomArrayImpl{" +
                "data=" + Arrays.toString(data) +
                ", size=" + size +
                '}';
    }
}
