package ru.sber.homework;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class CustomArrayImplTest {

    @Test
    public void size_Test() {
        CustomArrayImpl<Integer> list = new CustomArrayImpl<>();
        list.add(1);
        list.add(2);
        list.add(3);
        int result = list.size();
        Assert.assertEquals(3, result);
    }

    @Test
    public void isEmpty_Test() {
        CustomArrayImpl<Integer> list = new CustomArrayImpl<>(3);
        list.add(1);
        Assert.assertEquals(false,list.isEmpty());
    }

    @Test
    public void addOneElement_Test() {
        CustomArrayImpl<Integer> list = new CustomArrayImpl<>();
        list.add(2);
        Assert.assertEquals(1,list.size());
    }

    @Test
    public void addAllArray_Test() {
        CustomArrayImpl<Integer> cai = new CustomArrayImpl<>(5);
        cai.add(4);
        cai.add(5);
        cai.add(7);
        Integer[] number = new Integer[]{1,2};
        cai.addAll(number);
        Assert.assertEquals(5, cai.size());
    }

    @Test
    public void testAddAllCollection() {
        CustomArrayImpl<Integer> cai = new CustomArrayImpl<>(3);
        cai.add(4);
        cai.add(5);
        cai.add(7);
        List<Integer> list = new ArrayList<>(3);
        list.add(1);
        list.add(2);
        list.add(3);
        cai.addAll(list);
        Assert.assertEquals(6, cai.size());
    }

    @Test
    public void testAddAllByIndex() {
        CustomArrayImpl<Integer> cai = new CustomArrayImpl<>(3);
        cai.add(4);
        cai.add(5);
        cai.add(7);
        Integer[] number = new Integer[]{1,2};
        cai.addAll(2, number);
        Assert.assertEquals(5, cai.size());
        int result = cai.get(2);
        Assert.assertEquals(1, result);
    }

    @Test(expected = RuntimeException.class)
    public void testAddAllByIndexWhenTrowException() {
        CustomArrayImpl<Integer> cai = new CustomArrayImpl<>(3);
        cai.add(4);
        cai.add(5);
        cai.add(7);
        Integer[] number = new Integer[]{1,2};
        cai.addAll(-2, number);
    }

    @Test
    public void get_Test() {
        CustomArrayImpl<Integer> cai = new CustomArrayImpl<>(3);
        cai.add(4);
        cai.add(5);
        cai.add(7);
        int result = cai.get(2);
        Assert.assertEquals(7, result);
    }

    @Test(expected = RuntimeException.class)
    public void getWhenTrowException_Test() {
        CustomArrayImpl<Integer> cai = new CustomArrayImpl<>(3);
        cai.add(4);
        cai.get(-2);
    }

    @Test
    public void set_test() {
        CustomArrayImpl<String> arrayList = new CustomArrayImpl<>(5);
        arrayList.add("1");
        arrayList.add("2");
        arrayList.add("3");
        arrayList.add("4");
        arrayList.add("5");
        String s = arrayList.set(2,"6");
        Assert.assertEquals("6", s);
    }

    @Test(expected = RuntimeException.class)
    public void setWhenTrowException_Test() {
        CustomArrayImpl<String> arrayList = new CustomArrayImpl<>(5);
        arrayList.add("1");
        arrayList.add("2");
        String s = arrayList.set(2,"6");
    }

    @Test
    public void removeByIndex_Test() {
        CustomArrayImpl<String> arrayList = new CustomArrayImpl<>(5);
        arrayList.add("1");
        arrayList.add("2");
        arrayList.add("3");
        arrayList.add("4");
        arrayList.add("5");
        arrayList.remove(2);
        Assert.assertEquals(4, arrayList.size());
    }

    @Test(expected = RuntimeException.class)
    public void removeByIndexWhenTrowException_Test() {
        CustomArrayImpl<String> arrayList = new CustomArrayImpl<>(5);
        arrayList.add("1");
        arrayList.add("2");
        arrayList.remove(2);
        Assert.assertEquals(1,arrayList.size());
    }

    @Test
    public void testRemoveByValue() {
        CustomArrayImpl<String> arrayList = new CustomArrayImpl<>(5);
        arrayList.add("1");
        arrayList.add("2");
        arrayList.add("3");
        arrayList.add("4");
        arrayList.add("5");
        boolean result = arrayList.remove("1");
        Assert.assertEquals(true, result);
    }

    @Test
    public void contains_Test() {
        CustomArrayImpl<String> arrayList = new CustomArrayImpl<>(2);
        arrayList.add("1");
        arrayList.add("2");
        boolean result = arrayList.contains("2");
        Assert.assertEquals(true, result);
    }

    @Test
    public void indexOf_Test() {
        CustomArrayImpl<String> arrayList = new CustomArrayImpl<>(5);
        arrayList.add("1");
        arrayList.add("2");
        int number = arrayList.indexOf("1");
    }

    @Test
    public void ensureCapacity_test() {
        CustomArrayImpl<Integer> cai = new CustomArrayImpl<>(2);
        cai.add(4);
        cai.add(5);
        List<Integer> list = new ArrayList<>(3);
        list.add(1);
        list.add(2);
        list.add(3);
        cai.addAll(list);
        Assert.assertEquals(5, cai.size());
    }

    @Test
    public void getCapacity_test() {
        CustomArrayImpl<Integer> cai = new CustomArrayImpl<>();
        int result = cai.getCapacity();
        Assert.assertEquals(10, result);
        Assert.assertEquals(0, cai.size());
    }

    @Test
    public void reverse_Test() {
        CustomArrayImpl<String> arrayList = new CustomArrayImpl<>(3);
        arrayList.add("1");
        arrayList.add("2");
        arrayList.add("3");
        arrayList.reverse();
        Assert.assertEquals("3", arrayList.get(0));
        Assert.assertEquals("1", arrayList.get(2));
    }

    @Test
    public void toArray_Test() {
        CustomArrayImpl<String> arrayList = new CustomArrayImpl<>(3);
        arrayList.add("1");
        arrayList.add("2");
        arrayList.add("3");
        Object[] obj = arrayList.toArray();
        Assert.assertEquals("2", obj[1]);
    }
}